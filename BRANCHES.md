# Branching Strategy

## Summary

This document explains changes to the branching strategy used to manage
this repository.

There are four branches tracking the tree in this repository:

* `master`: Stable/Production-ready branch for feature releases.
* `maint`: Maintenance branch for feature releases (short-lived).
* `next`: Updates that are likely to be part of a future release.
* `pu`: Proposed updates with topic branches under development.

Generally, a topic must follow this promotion "path" to find its way
to a feature release: `topic → pu → next → master`.


## Branches in Detail

### `master`

The `master` branch is meant to contain commits that are well tested and
ready to be used in a production setting. From time to time, a "feature
release" is cut from the tip of this branch and it'll be named with
three-dotted decimal digits that end with `.0` (e.g. `1.3.0`) in a
`MAJOR.MINOR.PATCH` format[^1]. You can expect the tip of the `master`
branch to always be more stable than any of the released versions.


### `maint`

The `maint` branch is forked off from `master` whenever a feature release
is made. Obvious and safe fixes after a feature release are applied to
this branch and maintenance releases are cut from it. Usually the fixes
are merged to `master` first, some time before being merged to `maint`, to
reduce the chance of last-minute issues. Maintenance releases are named by
incrementing the last digit of three-dotted decimal name (e.g. `1.10.1`
would be the first maintenance release of the `1.10` series). Given the
nature of this system and its intended audience, maintenance releases are
only maintained for a short time, which is expected to coincide with the
duration of a single semester. The `maint` branch never gets new features
merged to it.


### `next`

The `next` branch is where topic branches that are in good shape are
merged to from `pu`. Generally, `next` always has the tip of `master`,
but may not be as stable. It's simply expected to work without major
issues. Topic branches that have been merged to `next` are expected to
be refined into a production-ready state before being merged to `master`.
You can help this process by using the `next` branch for your daily work,
and reporting any new bugs you find before the issues are merged to
`master`. The two branches `master` and `maint` are never rewound, and
`next` usually will not be either. After a feature release is made from
`master`, however, `next` will be rebased to the tip of `master`, carrying
over the topics that didn't make it into the most recent feature release.

Release candidates may be cut from the tip of this branch and will be
named using the same format as a feature release, but ending in `-rcN`,
where `N` is a number starting at `1`. For example, `0.3.0-rc1` woud be
the first release candidate for the upcoming `0.3.0` feature release,
`0.3.0-rc2` would be the second release candidate, and so on.

Being in `next` does not guarantee that the change(s) will be part of a
future release. Sometimes topic branches may need to have some commits in
them reverted before being ready to be merged to `master`. Topics that
have already been merged to `next` may need to be reverted in `next` due
to more severe problems being found after merging, or maybe we realized
that what seemed to be a good idea at one point turned out to have some
fatal flaws.


### `pu`

The `pu` (proposed updates) branch bundles all the remaining topic
branches I happen be aware of. There is no guarantee that there will be
enough bandwidth to pick up any and all topics that are remotely promising
from the list traffic, so please do not read too much into a topic being
on (or not on) the `pu` branch. This branch is mainly to remind myself
that the topics in them may turn out to be interesting when they are
polished. The topics on this branch aren't usually complete, well tested,
or well documented and they often need further work. When a topic that
is in `pu` proves to be in a more testable state, it is merged to `next`.

You can run `git log --first-parent master..pu` to see what topics are
currently under consideration. Sometimes, an idea that seemed promising
turns out to not be so good and the topic can be dropped from `pu` in
that case.


## Topic Branches

Development takes place in topic branches, which should be prefixed with
their intended purposes. For example, a topic branch for a new feature
should be prefixed with `feature/...`, one for fixing issues with
`fix/...`, documentation updates with `doc/...`, and so on.

Merge requests for topic branches should target the `pu` branch. A topic
branch is meant to be amended and replaced frequently with improvements
as development progress is made, but only as long as it has not been
merged to `next`. Once a topic branch is merged to `next`, subsequent
updates must come as incremental patches pointing out the issues in the
previous patch(es) and how they were fixed.


[^1]: https://semver.org
