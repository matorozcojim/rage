/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene;

import java.awt.*;
import java.awt.event.*;
import java.io.*;

import ray.rage.*;
import ray.rage.asset.texture.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.rendersystem.states.*;
import ray.rage.scene.controllers.*;
import ray.rml.*;

public class CameraNodeMovementTest extends VariableFrameRateGame {

    private SceneNode           activeEntityNode;
    private SceneNode           cameraNode;

    private SceneNode           earthNode;
    private SceneNode           moonNode;

    private final static String EARTH_ENTITY_NAME = "Earth";
    private final static String MOON_ENTITY_NAME  = "Moon";
    private final static String EARTH_NODE_NAME   = EARTH_ENTITY_NAME + "Node";
    private final static String MOON_NODE_NAME    = MOON_ENTITY_NAME + "Node";

    private final static float  MOVE_OFFSET       = 0.5f;
    private final static float  SCALE_UP          = 1.1f;
    private final static float  SCALE_DOWN        = 0.9f;
    private final Angle         rotationAngle     = new Degreef(5.5f);

    public CameraNodeMovementTest() {
        super();
    }

    public static void main(String[] args) {
        Game game = new CameraNodeMovementTest();
        try {
            game.startup();
            game.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            game.shutdown();
            game.exit();
        }
    }

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.setLocalPosition(0, 0, 12);
    }

    @Override
    protected void setupScene(Engine eng, SceneManager sm) throws IOException {
        // let the earth entity use the texture specified by the model/material
        final String meshFilename = "earth.obj";
        Entity earthEntity = sm.createEntity(EARTH_ENTITY_NAME, meshFilename);
        Entity moonEntity = sm.createEntity(MOON_ENTITY_NAME, meshFilename);

        // load a texture to set it manually
        TextureManager tm = eng.getTextureManager();
        Texture moonTexture = tm.getAssetByPath("moon.jpeg");

        RenderSystem rs = sm.getRenderSystem();
        TextureState state = (TextureState) rs.createRenderState(RenderState.Type.TEXTURE);
        state.setTexture(moonTexture);
        moonEntity.setRenderState(state);

        SceneNode rootNode = sm.getRootSceneNode();
        earthNode = rootNode.createChildSceneNode(EARTH_NODE_NAME);
        moonNode = rootNode.createChildSceneNode(MOON_NODE_NAME);

        Light sunLight = sm.createLight("SunLight", Light.Type.DIRECTIONAL);
        sunLight.setDiffuse(Color.WHITE);
        SceneNode sunLightNode = rootNode.createChildSceneNode(sunLight.getName() + "Node");
        sunLightNode.attachObject(sunLight);
        sunLightNode.setLocalPosition(1, 0, 1);

        earthNode.attachObject(earthEntity);
        earthNode.setLocalPosition(0, 0, -2);
        moonNode.attachObject(moonEntity);
        moonNode.setLocalScale(.33f, .33f, .33f);
        activeEntityNode = earthNode;

        // We could easily make the moon orbit around the earth by making it a
        // child node of the earth node and rotating the earth node, but I'm
        // updating this to use controllers to show an alternative and also set
        // different rotation/orbit speeds, which would not be possible without
        // the controllers. It's not meant to be accurate, since the moon is
        // tidally locked relative to earth and wouldn't let me use the
        // controllers to show that orbital and rotational speeds can be kept
        // independent from each other
        RotationController rc = new RotationController();
        rc.addNode(earthNode);
        rc.addNode(moonNode);
        rc.setSpeed(0.015f);

        OrbitController oc = new OrbitController(earthNode);
        oc.addNode(moonNode);
        oc.setSpeed(.4f);
        oc.setDistanceFromTarget(11);
        oc.setVerticalDistance(.5f);

        sm.addController(rc);
        sm.addController(oc);
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void shutdown(Engine engine) {
        engine.getSceneManager().destroyAllSceneObjects();
        engine.getRenderSystem().getRenderWindow().removeAllViewports();
        super.shutdown(engine);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                cameraNode.moveForward(MOVE_OFFSET);
                break;
            case KeyEvent.VK_A:
                cameraNode.moveLeft(MOVE_OFFSET);
                break;
            case KeyEvent.VK_S:
                cameraNode.moveBackward(MOVE_OFFSET);
                break;
            case KeyEvent.VK_D:
                cameraNode.moveRight(MOVE_OFFSET);
                break;
            case KeyEvent.VK_Q:
                cameraNode.roll(rotationAngle.negate());
                break;
            case KeyEvent.VK_E:
                cameraNode.roll(rotationAngle);
                break;
            case KeyEvent.VK_Z:
                cameraNode.pitch(rotationAngle);
                break;
            case KeyEvent.VK_X:
                cameraNode.pitch(rotationAngle.negate());
                break;
            case KeyEvent.VK_C:
                cameraNode.yaw(rotationAngle);
                break;
            case KeyEvent.VK_V:
                cameraNode.yaw(rotationAngle.negate());
                break;
            case KeyEvent.VK_H:
                SceneObject obj = activeEntityNode.getAttachedObject(0);
                obj.setVisible(!obj.isVisible());
                break;
            case KeyEvent.VK_1:
                activeEntityNode = getEngine().getSceneManager().getSceneNode(EARTH_NODE_NAME);
                break;
            case KeyEvent.VK_2:
                activeEntityNode = getEngine().getSceneManager().getSceneNode(MOON_NODE_NAME);
                break;
            case KeyEvent.VK_6:
                activeEntityNode.translate(MOVE_OFFSET, 0f, 0f);
                break;
            case KeyEvent.VK_7:
                activeEntityNode.translate(-MOVE_OFFSET, 0f, 0f);
                break;
            case KeyEvent.VK_LEFT:
                activeEntityNode.roll(rotationAngle);
                break;
            case KeyEvent.VK_L:
                cameraNode.lookAt(activeEntityNode);
                break;
            case KeyEvent.VK_RIGHT:
                activeEntityNode.roll(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_UP:
                activeEntityNode.pitch(rotationAngle.mult(-1));
                break;
            case KeyEvent.VK_DOWN:
                activeEntityNode.pitch(rotationAngle);
                break;
            case KeyEvent.VK_ADD:
                activeEntityNode.scale(SCALE_UP);
                break;
            case KeyEvent.VK_SUBTRACT:
                activeEntityNode.scale(SCALE_DOWN);
                break;
        }
        super.keyPressed(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getWheelRotation() < 0)
            cameraNode.moveUp(MOVE_OFFSET);
        else
            cameraNode.moveDown(MOVE_OFFSET);

        super.mouseWheelMoved(e);
    }

}
