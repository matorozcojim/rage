/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.util;

import static org.testng.AssertJUnit.*;

import java.io.*;

import org.testng.annotations.*;

public class ConfigurationTest {

    @Test
    public void testDefaultConfigurationCanBeRead() throws IOException {
        Configuration cfg = new Configuration();
        cfg.load();

        final String meshes = cfg.valueOf("assets.meshes.path");
        final String textures = cfg.valueOf("assets.textures.path");
        final String materials = cfg.valueOf("assets.materials.path");

        assertTrue(meshes != null);
        assertTrue(textures != null);
        assertTrue(materials != null);

        assertFalse(meshes.isEmpty());
        assertFalse(textures.isEmpty());
        assertFalse(materials.isEmpty());
    }

    @Test
    public void testCustomConfigurationCanBeRead() throws IOException {
        final String properties = "assets/config/rage.properties".replace('/', File.separatorChar);
        Configuration cfg = new Configuration();
        cfg.load(properties);

        final String meshes = cfg.valueOf("assets.meshes.path");
        final String textures = cfg.valueOf("assets.textures.path");
        final String materials = cfg.valueOf("assets.materials.path");

        assertTrue(meshes != null);
        assertTrue(textures != null);
        assertTrue(materials != null);

        assertFalse(meshes.isEmpty());
        assertFalse(textures.isEmpty());
        assertFalse(materials.isEmpty());
    }

    @Test(expectedExceptions = { FileNotFoundException.class })
    public void testNonExistentConfigurationFileFails() throws IOException {
        Configuration cfg = new Configuration();
        cfg.load("nonexistent.properties");
    }

    @Test
    public void testReadConfigurationValueMatchesExpected() throws IOException {
        Configuration cfg = new Configuration();
        cfg.load();
        final String loadedPath = cfg.valueOf("assets.meshes.path");
        final String expectedPath = "assets/meshes/".replace('/', File.separatorChar);

        assertEquals(loadedPath, expectedPath);
    }

    @Test
    public void testNewConfigurationValueMatches() throws IOException {
        // no need to load properties before setting them
        final String key = "new-key";
        final String val = "new/value";
        Configuration cfg = new Configuration();
        cfg.setKeyValuePair(key, val);

        assertEquals(cfg.valueOf(key), val);
    }

}
