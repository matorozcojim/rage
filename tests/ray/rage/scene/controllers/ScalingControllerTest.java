/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.scene.controllers;

import java.awt.*;
import java.io.*;

import ray.rage.*;
import ray.rage.game.*;
import ray.rage.rendersystem.*;
import ray.rage.scene.*;

public final class ScalingControllerTest extends VariableFrameRateGame {

    private ScalingController scalingController = new ScalingController();

    public static void main(String[] args) {
        Game test = new ScalingControllerTest();
        try {
            test.startup();
            test.run();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            test.shutdown();
            test.exit();
        }
    }

    @Override
    protected void update(Engine engine) {}

    @Override
    protected void setupCameras(SceneManager sm, RenderWindow rw) {
        Camera camera = sm.createCamera("MainCamera", Camera.Frustum.Projection.PERSPECTIVE);
        camera.getFrustum().setNearClipDistance(0.1f);
        camera.setViewport(rw.getViewport(0));

        SceneNode cameraNode = sm.getRootSceneNode().createChildSceneNode("CameraNode");
        cameraNode.attachObject(camera);
        cameraNode.moveBackward(10);
    }

    @Override
    protected void setupScene(Engine engine, SceneManager sm) throws IOException {
        sm.getAmbientLight().setIntensity(Color.LIGHT_GRAY);

        Entity sphere = sm.createEntity("Sphere", "sphere.obj");
        Entity cube = sm.createEntity("Cube", "cube.obj");

        SceneNode rootNode = sm.getRootSceneNode();
        SceneNode sphereNode = rootNode.createChildSceneNode(sphere.getName() + "Node");
        SceneNode cubeNode = rootNode.createChildSceneNode(cube.getName() + "Node");

        sphereNode.attachObject(sphere);
        cubeNode.attachObject(cube);

        sphereNode.moveRight(5);
        cubeNode.moveLeft(5);

        // make sure that both objects are scaled independently, i.e. that each
        // object gets scaled relative to its own scaling rather than all nodes
        // being set to the same size; here, the cube should always remain
        // smaller relative to the sphere
        sphereNode.scale(1.25f);
        cubeNode.scale(.75f);

        scalingController.addNode(sphereNode);
        scalingController.addNode(cubeNode);

        sm.addController(scalingController);
    }

}
