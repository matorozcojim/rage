/**
 * This file is part of RAGE.
 *
 * RAGE is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * RAGE is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package ray.rage.assets;

import static org.testng.AssertJUnit.*;

import org.testng.annotations.*;

import ray.rage.asset.material.*;

public class MaterialManagerTest {

    private static final String ASSET_NAME = "MainAsset";

    @Test
    public void testCanCreateManualMaterial() {
        MaterialManager mm = new MaterialManager();
        Material m = mm.createManualAsset(ASSET_NAME);

        assertEquals(ASSET_NAME, m.getName());
    }

    @Test(
        expectedExceptions = { RuntimeException.class },
        expectedExceptionsMessageRegExp = "Manual asset already exists: " + ASSET_NAME)
    public void testThrowsExceptionOnDuplicateMaterial() {
        MaterialManager mm = new MaterialManager();
        mm.createManualAsset(ASSET_NAME);
        mm.createManualAsset(ASSET_NAME);
    }

    @Test
    public void testFindsMaterialByName() {
        MaterialManager mm = new MaterialManager();
        mm.createManualAsset(ASSET_NAME);

        assertTrue(mm.hasAssetByName(ASSET_NAME));
    }

    @Test
    public void testDoesNotFindMaterialByName() {
        MaterialManager mm = new MaterialManager();
        assertFalse(mm.hasAssetByName(ASSET_NAME));
    }

    @Test
    public void testMeshCountMatches() {
        MaterialManager mm = new MaterialManager();

        assertEquals(0, mm.getAssetCount());
        mm.createManualAsset(ASSET_NAME);
        assertEquals(1, mm.getAssetCount());
    }

}
